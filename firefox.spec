# Set for local builds only
%global disable_toolsets  0

%global build_with_clang  0
%ifarch i686
# no debug package for the i686 because oom on i686 with debuginfos
%global debug_package %{nil}
%endif

%global rhel_minor_version -1
%if 0%{?flatpak:1}
%global rhel_minor_version 4
%endif
%if "%{?dist}" == ".el8"
%global rhel_minor_version 4
%endif
%if "%{?dist}" == ".el8_3"
%global rhel_minor_version 3
%endif
%if "%{?dist}" == ".el8_2"
%global rhel_minor_version 2
%endif
%if "%{?dist}" == ".el8_1"
%global rhel_minor_version 1
%endif
%if "%{?dist}" == ".el8_0"
%global rhel_minor_version 0
%endif
%if "%{?dist}" == ".el9"
%global rhel_minor_version 4
%endif

%global system_nss        1
%global bundle_nss        0

%if 0%{?rhel} >= 8
  %if 0%{?rhel_minor_version} < 2
%global bundle_nss        1
  %endif
%endif

%define use_bundled_ffi   0

%define use_bundled_python_2 1
%define use_bundled_python_3 1

%if 0%{?rhel} >= 8
%define use_bundled_python_2 1
%define use_bundled_python_3 0
%endif

%if 0%{?rhel} == 7
%define use_bundled_python_2 0
%define use_bundled_python_3 0
%endif

%if 0%{?flatpak:1}
%define use_bundled_python_2 1
%endif

# we need python2 because of icu data gen
%define use_bundled_python_2 0

%define bundle_gnome_extension 0

# Don't use system hunspell for now
%global system_hunspell   0
%global system_sqlite     0
%if 0%{?rhel} >= 8
%global use_llvmts        0
%else
%global use_llvmts        1
%endif

%global system_ffi        1
%if 0%{?rhel} < 8
%global use_dts           1
%endif

%global use_rustts        1
%global dts_version       8
%global rust_version         1.41
%global rust_toolset_version 1.41
%global llvm_version      7.0
%if 0%{?rhel} >= 8
%global llvm_version      6.0
%endif

%if 0%{?disable_toolsets}
%global use_rustts        0
%global use_dts           0
%global use_llvmts        0
%endif

# Use system cairo?
%global system_cairo      0

# Use system libvpx?
%global system_libvpx     0

# Use system libicu?
%global system_libicu     0

# Big endian platforms
%ifarch ppc64 s390x
# Javascript Intl API is not supported on big endian platforms right now:
# https://bugzilla.mozilla.org/show_bug.cgi?id=1322212
%global big_endian        1
%endif

# Hardened build?
%global hardened_build    1

%global system_jpeg       1

%ifarch %{ix86} x86_64
%global run_tests         0
%else
%global run_tests         0
%endif

# Build as a debug package?
%global debug_build       0

#%global default_bookmarks_file  %{_datadir}/bookmarks/default-bookmarks.html
# need to use full path because of flatpak where datadir is /app/share
%global default_bookmarks_file  /usr/share/bookmarks/default-bookmarks.html
%global firefox_app_id  \{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}
# Minimal required versions
%global cairo_version 1.13.1
%global freetype_version 2.1.9
%if %{?system_libvpx}
%global libvpx_version 1.4.0
%endif

%if 0%{?system_nss}
%global nspr_version 4.25
# NSS/NSPR quite often ends in build override, so as requirement the version
# we're building against could bring us some broken dependencies from time to time.
#%global nspr_build_version %(pkg-config --silence-errors --modversion nspr 2>/dev/null || echo 65536)
%global nspr_build_version %{nspr_version}
%global nss_version 3.53.1
#%global nss_build_version %(pkg-config --silence-errors --modversion nss 2>/dev/null || echo 65536)
%global nss_build_version %{nss_version}
%endif

%if %{?system_sqlite}
%global sqlite_version 3.8.4.2
# The actual sqlite version (see #480989):
%global sqlite_build_version %(pkg-config --silence-errors --modversion sqlite3 2>/dev/null || echo 65536)
%endif

%define bundled_python_version_2 2.7.13
%define bundled_python_version_3 3.6.8
%define use_bundled_openssl     0
%define use_bundled_nodejs      0
%define use_bundled_yasm        0

%if 0%{?rhel} >= 8
  %if 0%{?rhel_minor_version} <= 2
%define use_bundled_nodejs      1
  %endif
%endif

%if 0%{?rhel} == 7
%define use_bundled_nodejs      1
%define use_bundled_yasm        1
%endif

# GTK3 bundling
%define avoid_bundled_rebuild   0

%define gtk3_nvr 3.22.26-1
%define gtk3_install_path %{mozappdir}/bundled

# We could use %%include, but in %%files, %%post and other sections, but in these
# sections it could lead to syntax errors about unclosed %%if. Work around it by
# using the following macro
%define include_file() %{expand:%(cat '%1')}

%global mozappdir     %{_libdir}/%{name}
%global mozappdirdev  %{_libdir}/%{name}-devel-%{version}
%global langpackdir   %{mozappdir}/langpacks
%global tarballdir    %{name}-%{version}
%global pre_version   esr
#global pre_tag       alpha

%global official_branding       1
%global build_langpacks         1

%global enable_mozilla_crashreporter       0
%if !%{debug_build}
%ifarch %{ix86} x86_64
%global enable_mozilla_crashreporter       0
%endif
%endif

Summary:        Mozilla Firefox Web browser
Name:           firefox
Version:        78.8.0
Release:        5%{?dist}
URL:            https://www.mozilla.org/firefox/
License:        MPLv1.1 or GPLv2+ or LGPLv2+
%if 0%{?rhel} == 7
ExcludeArch:    s390 ppc
%endif

Source0:        https://hg.mozilla.org/releases/mozilla-release/archive/firefox-%{version}%{?pre_version}.source.tar.xz
%if %{build_langpacks}
Source1:        firefox-langpacks-%{version}%{?pre_version}-20210222.tar.xz
%endif
Source2:        cbindgen-vendor-0.14.3.tar.xz
Source10:       firefox-mozconfig
Source12:       firefox-redhat-default-prefs.js
Source20:       firefox.desktop
Source21:       firefox.sh.in
Source23:       firefox.1
Source24:       mozilla-api-key
Source25:       firefox-symbolic.svg
Source26:       distribution.ini
Source27:       google-api-key
Source28:       node-stdout-nonblocking-wrapper

Source200:      gtk3-private-%{gtk3_nvr}.el6.src.rpm
Source201:      gtk3-private-%{gtk3_nvr}-post.inc
Source202:      gtk3-private-%{gtk3_nvr}-postun.inc
Source203:      gtk3-private-%{gtk3_nvr}-posttrans.inc
Source204:      gtk3-private-%{gtk3_nvr}-files.inc
Source205:      gtk3-private-%{gtk3_nvr}-setup-flags-env.inc
Source206:      gtk3-private-%{gtk3_nvr}-requires-provides-filter.inc
Source301:      yasm-1.2.0-3.el5.src.rpm
Source303:      libffi-3.0.13-18.el7_3.src.rpm
Source304:      nodejs-10.21.0-5.fc32.src.rpm
Source305:      openssl-1.0.2k-19.6.bundle.el7_7.src.rpm

Source403:      nss-3.53.1-3.fc32.src.rpm
Source401:      nss-setup-flags-env.inc
Source402:      nspr-4.25.0-1.el8_0.src.rpm 
#Python
%if 0%{?use_bundled_python_2}
Source100:      https://www.python.org/ftp/python/%{bundled_python_version_2}/Python-%{bundled_python_version_2}.tar.xz
%endif
%if 0%{?use_bundled_python_3}
Source101:      https://www.python.org/ftp/python/%{bundled_python_version_3}/Python-%{bundled_python_version_3}.tar.xz
%endif
# Build patches
Patch1000:      python-2.7.patch
Patch1001:      build-ppc64le-inline.patch
Patch1002:      python-2.7-gcc8-fix.patch
Patch1003:      python-missing-utimensat.patch
Patch1004:      build-icu-make.patch
Patch1005:      mozilla-1692893-glib-object.patch
Patch1006:      D89554-autoconf1.diff
Patch1007:      D94538-autoconf2.diff
# workaround for https://bugzilla.redhat.com/show_bug.cgi?id=1699374
Patch4:         build-mozconfig-fix.patch
Patch6:         build-nss-version.patch
Patch7:         firefox-debugedits-error.patch

# Fedora/RHEL specific patches
Patch215:        firefox-enable-addons.patch
Patch219:        rhbz-1173156.patch
Patch224:        mozilla-1170092.patch
#ARM run-time patch
Patch231:        firefox-pipewire.patch
Patch232:        firefox-rhel6-hugepage.patch
Patch233:        firefox-rhel6-nss-tls1.3.patch
Patch234:        rhbz-1821418.patch
Patch235:        firefox-pipewire-0-3.patch

# Upstream patches
Patch402:        mozilla-1196777.patch

Patch501:        python-encode.patch
Patch503:        mozilla-s390-context.patch
Patch505:        mozilla-bmo1005535.patch
Patch506:        mozilla-bmo1504834-part1.patch
Patch507:        mozilla-bmo1504834-part2.patch
Patch508:        mozilla-bmo1504834-part3.patch
Patch509:        mozilla-bmo1504834-part4.patch
Patch510:        mozilla-bmo1554971.patch
Patch511:        mozilla-bmo1602730.patch
Patch512:        mozilla-bmo849632.patch
Patch513:        mozilla-bmo998749.patch
Patch514:        mozilla-s390x-skia-gradient.patch
Patch515:        mozilla-bmo1626236.patch
Patch516:        D87019-thin-vec-big-endian.diff

# Flatpak patches

%if %{?system_nss}
%if !0%{?bundle_nss}
BuildRequires:  pkgconfig(nspr) >= %{nspr_version}
BuildRequires:  pkgconfig(nss) >= %{nss_version}
BuildRequires:  nss-static >= %{nss_version}
%endif
%endif
%if %{?system_cairo}
BuildRequires:  pkgconfig(cairo) >= %{cairo_version}
%endif
BuildRequires:  pkgconfig(libpng)
BuildRequires:  xz
BuildRequires:  libXt-devel
BuildRequires:  mesa-libGL-devel
Requires:       liberation-fonts-common
Requires:       liberation-sans-fonts
%if %{?system_jpeg}
BuildRequires:  libjpeg-devel
%endif
BuildRequires:  zip
BuildRequires:  bzip2-devel
BuildRequires:  pkgconfig(zlib)
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  krb5-devel
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(freetype2) >= %{freetype_version}
BuildRequires:  pkgconfig(xt)
BuildRequires:  pkgconfig(xrender)
%if %{?system_hunspell}
BuildRequires:  hunspell-devel
%endif
BuildRequires:  pkgconfig(libstartup-notification-1.0)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(dri)
BuildRequires:  pkgconfig(libcurl)
BuildRequires:  dbus-glib-devel
%if %{?system_libvpx}
BuildRequires:  libvpx-devel >= %{libvpx_version}
%endif
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  m4

%if 0%{?use_dts}
BuildRequires:  devtoolset-%{dts_version}-gcc-c++
BuildRequires:  devtoolset-%{dts_version}-gcc
BuildRequires:  devtoolset-%{dts_version}-binutils
BuildRequires:  devtoolset-%{dts_version}-libatomic-devel
%if 0%{?use_llvmts}
BuildRequires:  llvm-toolset-%{llvm_version}
BuildRequires:  llvm-toolset-%{llvm_version}-llvm-devel
%endif
%endif

BuildRequires:  scl-utils
BuildRequires:  findutils


%if 0%{?rhel} >= 8
BuildRequires:  cargo
BuildRequires:  rust >= %{rust_version}
BuildRequires:  llvm >= %{llvm_version}
BuildRequires:  llvm-devel >= %{llvm_version}
BuildRequires:  clang >= %{llvm_version}
BuildRequires:  clang-devel >= %{llvm_version}
BuildRequires:  rustfmt >= %{rust_version}
BuildRequires:  python3
BuildRequires:  nodejs >= 10.21
%else
%if 0%{?use_rustts}
BuildRequires:  rust-toolset-%{rust_toolset_version}
%endif
%if 0%{?rhel} == 7
#BuildRequires:  rh-nodejs12
%endif
%if 0%{?use_llvmts}
BuildRequires:  llvm-toolset-%{llvm_version}
BuildRequires:  llvm-toolset-%{llvm_version}-llvm-devel
%endif
%endif

%if ! 0%{?use_bundled_yasm}
BuildRequires:  yasm
%endif

%if 0%{?use_bundled_python_2}
# Needed for Python in RHEL6
BuildRequires:  openssl-devel
%endif

%if 0%{?rhel} >= 8
  %if 0%{?rhel_minor_version} >= 3
BuildRequires:  pkgconfig(libpipewire-0.3)
  %else
BuildRequires:  pipewire-devel
  %endif
%endif

%if 0%{?bundle_gtk3}
BuildRequires:        automake
BuildRequires:        autoconf
BuildRequires:        cups-devel
BuildRequires:        dbus-devel
BuildRequires:        desktop-file-utils
BuildRequires:        expat-devel
BuildRequires:        fontpackages-devel
BuildRequires:        gamin-devel
BuildRequires:        gettext-devel
BuildRequires:        git
BuildRequires:        intltool
BuildRequires:        jasper-devel
BuildRequires:        libepoxy-devel
BuildRequires:        libcroco-devel
BuildRequires:        libffi-devel
BuildRequires:        libpng-devel
BuildRequires:        libtiff-devel
BuildRequires:        libtool
BuildRequires:        libxml2-devel
BuildRequires:        libX11-devel
BuildRequires:        libXcomposite-devel
BuildRequires:        libXcursor-devel
BuildRequires:        libXinerama-devel
BuildRequires:        libXevie-devel
BuildRequires:        libXrandr-devel
BuildRequires:        libXrender-devel
BuildRequires:        libXtst-devel
BuildRequires:        mesa-libGL-devel
BuildRequires:        mesa-libEGL-devel
BuildRequires:        pixman-devel
BuildRequires:        rest-devel
BuildRequires:        readline-devel
# TODO: We miss that dependency in our bundled gtk3 package.
# As a hotfix we put it here and fix gtk3 in next release.
Requires:             mesa-libEGL%{?_isa}
Requires:             libcroco%{?_isa}
Requires:             mesa-libGL%{?_isa}
Requires:             bzip2-libs%{?_isa}
Requires:             libXtst%{?_isa}
%else
BuildRequires:        gtk3-devel
BuildRequires:        glib2-devel
%endif

# Bundled nss/nspr requirement
%if 0%{?bundle_nss}
BuildRequires:    nss-softokn
BuildRequires:    sqlite-devel
BuildRequires:    zlib-devel
BuildRequires:    pkgconfig
BuildRequires:    gawk
BuildRequires:    psmisc
BuildRequires:    perl-interpreter
BuildRequires:    gcc-c++
BuildRequires:    xmlto
%endif
#RHEL9
BuildRequires:    perl-interpreter

Requires:       mozilla-filesystem
Requires:       p11-kit-trust
%if %{?system_nss}
%if !0%{?bundle_nss}
Requires:       nspr >= %{nspr_build_version}
Requires:       nss >= %{nss_build_version}
%endif
%endif

BuildRequires:  desktop-file-utils
BuildRequires:  system-bookmarks
Requires:       redhat-indexhtml
#for the python2
BuildRequires:  pkgconfig(sqlite3)
%if %{?system_sqlite}
BuildRequires:  pkgconfig(sqlite3) >= %{sqlite_version}
Requires:       sqlite >= %{sqlite_build_version}
%endif


%if %{?run_tests}
BuildRequires:  xorg-x11-server-Xvfb
%endif

%if %{?system_ffi}
  %if !%{use_bundled_ffi}0
BuildRequires:  pkgconfig(libffi)
  %endif
%endif

%if %{?use_bundled_nodejs}
%if !0%{?use_bundled_python_3}
BuildRequires: python3-devel
%endif
BuildRequires: zlib-devel
#BuildRequires: brotli-devel
#BuildRequires: gcc >= 4.9.4
#BuildRequires: gcc-c++ >= 4.9.4
BuildRequires: chrpath
BuildRequires: libatomic
BuildRequires: openssl-devel
%endif

%if 0%{?big_endian}
BuildRequires:  icu
%endif

Obsoletes:      mozilla <= 37:1.7.13
Provides:       webclient

%description
Mozilla Firefox is an open-source web browser, designed for standards
compliance, performance and portability.

%if %{enable_mozilla_crashreporter}
%global moz_debug_prefix %{_prefix}/lib/debug
%global moz_debug_dir %{moz_debug_prefix}%{mozappdir}
%global uname_m %(uname -m)
%global symbols_file_name %{name}-%{version}.en-US.%{_os}-%{uname_m}.crashreporter-symbols.zip
%global symbols_file_path %{moz_debug_dir}/%{symbols_file_name}
%global _find_debuginfo_opts -p %{symbols_file_path} -o debugcrashreporter.list
%global crashreporter_pkg_name mozilla-crashreporter-%{name}-debuginfo
%package -n %{crashreporter_pkg_name}
Summary: Debugging symbols used by Mozilla's crash reporter servers
%description -n %{crashreporter_pkg_name}
This package provides debug information for Firefox, for use by
Mozilla's crash reporter servers.  If you are trying to locally
debug %{name}, you want to install %{name}-debuginfo instead.
%files -n %{crashreporter_pkg_name} -f debugcrashreporter.list
%endif

%if %{run_tests}
%global testsuite_pkg_name mozilla-%{name}-testresults
%package -n %{testsuite_pkg_name}
Summary: Results of testsuite
%description -n %{testsuite_pkg_name}
This package contains results of tests executed during build.
%files -n %{testsuite_pkg_name}
/test_results
%endif

#---------------------------------------------------------------------

%if %{?bundle_gnome_extension}
%package        -n firefox-gnome-shell-extension
%global         firefox_gnome_shell_addon_name addon-751081-latest.xpi
Summary:        Support for managing GNOME Shell Extensions through web browsers
Requires:       %{name}%{?_isa} = %{version}-%{release}

License:        GPLv3+
URL:            https://wiki.gnome.org/Projects/GnomeShellIntegrationForChrome
Source2:        https://addons.mozilla.org/firefox/downloads/latest/gnome-shell-integration/platform:2/%{firefox_gnome_shell_addon_name}

Requires:       dbus
Requires:       gnome-icon-theme
Requires:       gnome-shell
Requires:       hicolor-icon-theme
Requires:       mozilla-filesystem

%description -n firefox-gnome-shell-extension
Browser extension for Firefox and native host messaging connector that provides
integration with GNOME Shell and the corresponding extensions repository
https://extensions.gnome.org.

%files -n firefox-gnome-shell-extension
%{mozappdir}/distribution/extensions/chrome-gnome-shell@gnome.org.xpi
%endif

%prep
echo "Build environment"
echo "dist                  %{?dist}"
echo "RHEL 8 minor version: %{?rhel_minor_version}"
echo "use_bundled_ffi       %{?use_bundled_ffi}"
echo "use_bundled_python_2  %{?use_bundled_python_2}"
echo "use_bundled_python_3  %{?use_bundled_python_3}"
echo "bundle_nss            %{?bundle_nss}"
echo "system_nss            %{?system_nss}"
echo "use_rustts            %{?use_rustts}"
echo "use_bundled_nodejs    %{?use_bundled_nodejs}"
echo "use_bundled_openssl   %{?use_bundled_openssl}"
echo "use_bundled_yasm      %{?use_bundled_yasm}"


%if 0%{?use_bundled_python_2}
%setup -q -T -c -n python2 -a 100
%patch1000 -p0 -b .build
%patch1002 -p0 -b .gcc8
%endif
%if 0%{?use_bundled_python_3}
%setup -q -T -c -n python3 -a 101
%endif
%setup -q -n %{tarballdir}
# Build patches, can't change backup suffix from default because during build
# there is a compare of config and js/config directories and .orig suffix is
# ignored during this compare.
%patch7 -p1 -b .debugedits-error
%ifarch %{ix86} %{arm} ppc
# binary check fails OOM on 32bit arches
%endif

%patch4  -p1 -b .build-mozconfig-fix
#%patch6  -p1 -b .nss-version

# Fedora patches
%patch215 -p1 -b .addons
%patch219 -p1 -b .rhbz-1173156
%patch224 -p1 -b .1170092

# fixing /usr/include in the patch for the flatpak build
%if 0%{?flatpak}
sed -ie 's|/usr/include|/app/include|' %_sourcedir/firefox-pipewire-0-3.patch
%endif

%if 0%{?rhel} >= 8
  %if 0%{?rhel_minor_version} >= 3
%patch235 -p1 -b .pipewire-0-3
  %else
%patch231 -p1 -b .pipewire
  %endif
%endif


%patch234 -p1 -b .rhbz-1821418

%patch402 -p1 -b .1196777

# Patch for big endian platforms only
%if 0%{?big_endian}
%endif

%patch501 -p1 -b .python-encode
%patch503 -p1 -b .mozilla-s390-context
%patch505 -p1 -b .mozilla-bmo1005535
%patch506 -p1 -b .mozilla-bmo1504834-part1
%patch507 -p1 -b .mozilla-bmo1504834-part2
%patch508 -p1 -b .mozilla-bmo1504834-part3
%patch509 -p1 -b .mozilla-bmo1504834-part4
%patch510 -p1 -b .mozilla-bmo1554971
%patch511 -p1 -b .mozilla-bmo1602730
%patch512 -p1 -b .mozilla-bmo849632
%patch513 -p1 -b .mozilla-bmo998749
%patch514 -p1 -b .mozilla-s390x-skia-gradient
%patch515 -p1 -b .mozilla-bmo1626236
%patch516 -p1 -b .D87019-thin-vec-big-endian.diff


%patch1001 -p1 -b .ppc64le-inline
%patch1004 -p1 -b .icu-make
%patch1005 -p1 -b .mozilla-1692893-glib-object
%patch1006 -p1 -b .D89554-autoconf1.diff
%patch1007 -p1 -b .D94538-autoconf2.diff

%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig
%if %{official_branding}
echo "ac_add_options --enable-official-branding" >> .mozconfig
%endif
%{__cp} %{SOURCE24} mozilla-api-key
%{__cp} %{SOURCE27} google-api-key

%if %{?system_nss}
echo "ac_add_options --with-system-nspr" >> .mozconfig
echo "ac_add_options --with-system-nss" >> .mozconfig
%else
echo "ac_add_options --without-system-nspr" >> .mozconfig
echo "ac_add_options --without-system-nss" >> .mozconfig
%endif

%if %{?system_cairo}
echo "ac_add_options --enable-system-cairo" >> .mozconfig
%else
echo "ac_add_options --disable-system-cairo" >> .mozconfig
%endif

%if 0%{?use_bundled_ffi}
echo "ac_add_options --enable-system-ffi" >> .mozconfig
%endif
%if 0%{?system_ffi}
echo "ac_add_options --enable-system-ffi" >> .mozconfig
%endif

%ifarch %{arm} %{ix86} x86_64
echo "ac_add_options --disable-elf-hack" >> .mozconfig
%endif

%if %{?system_hunspell}
echo "ac_add_options --enable-system-hunspell" >> .mozconfig
%else
# not available?
#echo "ac_add_options --disable-system-hunspell" >> .mozconfig
%endif

%if %{?debug_build}
echo "ac_add_options --enable-debug" >> .mozconfig
echo "ac_add_options --disable-optimize" >> .mozconfig
%else
%global optimize_flags "-g -O2"
%ifarch s390 s390x
%global optimize_flags "-g -O1"
%endif
%ifarch armv7hl
# ARMv7 need that (rhbz#1426850)
%global optimize_flags "-g -O2 -fno-schedule-insns"
%endif
%ifarch ppc64le aarch64
%global optimize_flags "-g -O2"
%endif
%if %{optimize_flags} != "none"
echo 'ac_add_options --enable-optimize=%{?optimize_flags}' >> .mozconfig
%else
echo 'ac_add_options --enable-optimize' >> .mozconfig
%endif
echo "ac_add_options --disable-debug" >> .mozconfig
%endif

# Second arches fail to start with jemalloc enabled
%ifnarch %{ix86} x86_64
echo "ac_add_options --disable-jemalloc" >> .mozconfig
%endif

%ifnarch %{ix86} x86_64
echo "ac_add_options --disable-webrtc" >> .mozconfig
%endif

%if !%{enable_mozilla_crashreporter}
echo "ac_add_options --disable-crashreporter" >> .mozconfig
%endif

%if %{?run_tests}
echo "ac_add_options --enable-tests" >> .mozconfig
%endif

%if !%{?system_jpeg}
echo "ac_add_options --without-system-jpeg" >> .mozconfig
%else
echo "ac_add_options --with-system-jpeg" >> .mozconfig
%endif

%if %{?system_libvpx}
echo "ac_add_options --with-system-libvpx" >> .mozconfig
%else
echo "ac_add_options --without-system-libvpx" >> .mozconfig
%endif

%if %{?system_libicu}
echo "ac_add_options --with-system-icu" >> .mozconfig
%else
echo "ac_add_options --without-system-icu" >> .mozconfig
%endif
%ifarch s390 s390x
echo "ac_add_options --disable-jit" >> .mozconfig
%endif

%ifnarch %{ix86}
%if !0%{?debug_build}
echo "ac_add_options --disable-debug-symbols" >> .mozconfig
%endif
%endif

echo 'export NODEJS="%{_buildrootdir}/bin/node-stdout-nonblocking-wrapper"' >> .mozconfig

# Remove executable bit to make brp-mangle-shebangs happy.
chmod -x third_party/rust/itertools/src/lib.rs
chmod a-x third_party/rust/gfx-backend-vulkan/src/*.rs
chmod a-x third_party/rust/gfx-hal/src/*.rs
chmod a-x third_party/rust/ash/src/extensions/ext/*.rs
chmod a-x third_party/rust/ash/src/extensions/khr/*.rs
chmod a-x third_party/rust/ash/src/extensions/mvk/*.rs
chmod a-x third_party/rust/ash/src/extensions/nv/*.rs

#---------------------------------------------------------------------

%build
# Disable LTO to work around rhbz#1883904
%define _lto_cflags %{nil}
ulimit -a
free
#set -e

%if ! 0%{?avoid_bundled_rebuild}
    rm -rf %{_buildrootdir}/*
%endif
export PATH="%{_buildrootdir}/bin:$PATH"

function install_rpms_to_current_dir() {
    PACKAGE_RPM=$(eval echo $1)
    PACKAGE_DIR=%{_rpmdir}

    if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
        # Hack for tps tests
        ARCH_STR=%{_arch}
        %ifarch i386 i686
            ARCH_STR="i?86"
        %endif
        PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
     fi

     for package in $(ls $PACKAGE_DIR/$PACKAGE_RPM)
     do
         echo "$package"
         rpm2cpio "$package" | cpio -idu
     done
}

function build_bundled_package() {
  PACKAGE_RPM=$1
  PACKAGE_FILES=$2
  PACKAGE_SOURCE=$3
  PACKAGE_BUILD_OPTIONS=$4
  export PACKAGE_DIR="%{_topdir}/RPMS"

  PACKAGE_ALREADY_BUILD=0
  %if %{?avoid_bundled_rebuild}
    if ls $PACKAGE_DIR/$PACKAGE_RPM; then
      PACKAGE_ALREADY_BUILD=1
    fi
    if ls $PACKAGE_DIR/%{_arch}/$PACKAGE_RPM; then
      PACKAGE_ALREADY_BUILD=1
    fi
  %endif
  if [ $PACKAGE_ALREADY_BUILD == 0 ]; then
    echo "Rebuilding $PACKAGE_RPM from $PACKAGE_SOURCE"; echo "==============================="
    rpmbuild --nodeps $PACKAGE_BUILD_OPTIONS --rebuild $PACKAGE_SOURCE
    cat /var/tmp/rpm-tmp*
  fi

  find $PACKAGE_DIR
  if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
    # Hack for tps tests
    ARCH_STR=%{_arch}
    %ifarch i386 i686
    ARCH_STR="i?86"
    %endif
    export PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
  fi
  pushd $PACKAGE_DIR

  echo "Installing $PACKAGE_DIR/$PACKAGE_RPM"; echo "==============================="
  pwd
  PACKAGE_LIST=$(echo $PACKAGE_DIR/$PACKAGE_RPM | tr " " "\n")
  for PACKAGE in $PACKAGE_LIST
  do
      rpm2cpio $PACKAGE | cpio -iduv
  done

  PATH=$PACKAGE_DIR/usr/bin:$PATH
  export PATH
  LD_LIBRARY_PATH=$PACKAGE_DIR/usr/%{_lib}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH

  # Clean rpms to avoid including them to package
  %if ! 0%{?avoid_bundled_rebuild}
    rm -f $PACKAGE_FILES
  %endif

  popd
}

# Build and install local yasm if needed
# ======================================
%if 0%{?use_bundled_yasm}
  build_bundled_package 'yasm-1*.rpm' 'yasm-*.rpm' '%{SOURCE301}'
%endif

%if 0%{?bundle_nss}
   rpm -ivh %{SOURCE402}
   #rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' --without=tests -ba %{_specdir}/nspr.spec
   rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/nspr.spec
   pushd %{_buildrootdir}
   install_rpms_to_current_dir nspr-4*.rpm
   install_rpms_to_current_dir nspr-devel*.rpm
   popd
   echo "Setting nspr flags"
   # nss-setup-flags-env.inc
   sed -i 's@%{gtk3_install_path}@%{_buildrootdir}%{gtk3_install_path}@g' %{_buildrootdir}%{gtk3_install_path}/%{_lib}/pkgconfig/nspr*.pc

   export LDFLAGS="-L%{_buildrootdir}%{gtk3_install_path}/%{_lib} $LDFLAGS"
   export LDFLAGS="-Wl,-rpath,%{gtk3_install_path}/%{_lib} $LDFLAGS"
   export LDFLAGS="-Wl,-rpath-link,%{_buildrootdir}%{gtk3_install_path}/%{_lib} $LDFLAGS"
   export PKG_CONFIG_PATH=%{_buildrootdir}%{gtk3_install_path}/%{_lib}/pkgconfig
   export PATH="{_buildrootdir}%{gtk3_install_path}/bin:$PATH"

   export PATH=%{_buildrootdir}/%{gtk3_install_path}/bin:$PATH
   echo $PKG_CONFIG_PATH

   rpm -ivh %{SOURCE403}
   rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/nss.spec
   pushd %{_buildrootdir}
   #cleanup
   #rm -rf {_buildrootdir}/usr/lib/debug/*
   #rm -rf {_buildrootdir}/usr/lib/.build-id
   #install_rpms_to_current_dir nss-%{gtk3_nvr}*.rpm
   #install_rpms_to_current_dir nss-devel-%{gtk3_nvr}*.rpm
   install_rpms_to_current_dir nss-3*.rpm
   install_rpms_to_current_dir nss-devel*.rpm
   install_rpms_to_current_dir nss-pkcs11-devel*.rpm
   install_rpms_to_current_dir nss-softokn-3*.rpm
   install_rpms_to_current_dir nss-softokn-devel*.rpm
   install_rpms_to_current_dir nss-softokn-freebl-3*.rpm
   install_rpms_to_current_dir nss-softokn-freebl-devel*.rpm
   install_rpms_to_current_dir nss-util-3*.rpm
   install_rpms_to_current_dir nss-util-devel*.rpm
   popd
  %filter_provides_in %{gtk3_install_path}/%{_lib}
  %filter_requires_in %{gtk3_install_path}/%{_lib}
  %filter_from_requires /libnss3.so.*/d
  %filter_from_requires /libsmime3.so.*/d
  %filter_from_requires /libssl3.so.*/d
  %filter_from_requires /libnssutil3.so.*/d
  %filter_from_requires /libnspr4.so.*/d
%endif

%if 0%{?bundle_gtk3}
   %if ! 0%{?avoid_bundled_rebuild}
    rpm -ivh %{SOURCE200}
    rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/gtk3-private.spec
   %endif
   pushd %{_buildrootdir}
   install_rpms_to_current_dir gtk3-private-%{gtk3_nvr}*.rpm
   install_rpms_to_current_dir gtk3-private-devel-%{gtk3_nvr}*.rpm
   install_rpms_to_current_dir gtk3-private-rpm-scripts-%{gtk3_nvr}*.rpm
   popd
%endif

%if 0%{?bundle_gtk3}
# gtk3-private-3.22.26.el6-1-requires-provides-filter.inc
%include_file %{SOURCE206}
%endif
%if 0%{use_bundled_ffi}
  # Install libraries to the predefined location to later add them to the Firefox libraries
  rpm -ivh %{SOURCE303}
  rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/libffi.spec
  pushd %{_buildrootdir}
  install_rpms_to_current_dir 'libffi*.rpm'
  popd
  %filter_from_requires /libffi.so.6/d
%endif
%filter_setup

# If needed build the bundled python 2.7 and 3.6 and put it in the PATH
%if 0%{?use_bundled_python_3}
    pushd %{_builddir}/python3/Python-%{bundled_python_version_3}
    ./configure --prefix="%{_buildrootdir}" --exec-prefix="%{_buildrootdir}" --libdir="%{_buildrootdir}/lib" || cat config.log
    make %{?_smp_mflags} install V=1 -j1
    cp Tools/scripts/pathfix.py %{_buildrootdir}/bin
    popd
%endif
%if 0%{?use_bundled_python_2}
    pushd %{_builddir}/python2/Python-%{bundled_python_version_2}
    ./configure --prefix="%{_buildrootdir}" --exec-prefix="%{_buildrootdir}" --libdir="%{_buildrootdir}/lib" || cat config.log
    make %{?_smp_mflags} install V=1
    popd    
%endif

function replace_prefix() {
  FILE_NAME=$1
  PKG_CONFIG_PREFIX=$2

  cat $FILE_NAME | tail -n +2 > tmp.txt
  echo "$PKG_CONFIG_PREFIX" > $FILE_NAME
  cat tmp.txt >> $FILE_NAME
  rm -rf tmp.txt
}

# Build and install local openssl if needed
# =========================================
%if 0%{?use_bundled_openssl}
  rpm -ivh %{SOURCE305}
  rpmbuild --nodeps -ba %{_specdir}/openssl.spec
  pushd %{_buildrootdir}
  install_rpms_to_current_dir openssl-1.0.2k*.rpm
  install_rpms_to_current_dir openssl-libs-1.0.2k*.rpm
  install_rpms_to_current_dir openssl-devel-1.0.2k*.rpm
  install_rpms_to_current_dir openssl-static-1.0.2k*.rpm
  # openssl is installed to %{_buildrootdir}/usr/lib(64)/...
  export PKG_CONFIG_PATH=%{_buildrootdir}/%{_libdir}/pkgconfig/:$PKG_CONFIG_PATH
  replace_prefix %{_buildrootdir}/%{_libdir}/pkgconfig/libcrypto.pc prefix=%{_buildrootdir}/usr
  replace_prefix %{_buildrootdir}/%{_libdir}/pkgconfig/libssl.pc prefix=%{_buildrootdir}/usr
  replace_prefix %{_buildrootdir}/%{_libdir}/pkgconfig/openssl.pc prefix=%{_buildrootdir}/usr
  cat  %{_buildrootdir}/%{_libdir}/pkgconfig/libcrypto.pc
  cat  %{_buildrootdir}/%{_libdir}/pkgconfig/libssl.pc
  cat  %{_buildrootdir}/%{_libdir}/pkgconfig/openssl.pc
  pushd %{_rpmdir}
  rm -f openssl-*.rpm
  popd
  popd
%endif

# We need to disable exit on error temporarily for the following scripts:
set +e
%if 0%{?use_dts}
source scl_source enable devtoolset-%{dts_version}
%endif
%if 0%{?use_rustts}
source scl_source enable rust-toolset-%{rust_toolset_version}
%endif

env
which gcc
which c++
which g++
which ld
# Build and install local node if needed
# ======================================
%if %{use_bundled_nodejs}
  build_bundled_package 'nodejs-10*.rpm' 'nodejs-*.rpm npm-*.rpm' %{SOURCE304} "--with bootstrap"
  export MOZ_NODEJS=$PACKAGE_DIR/usr/bin/node
%else
  export MOZ_NODEJS=/usr/bin/node
%endif

mkdir -p my_rust_vendor
cd my_rust_vendor
%{__tar} xf %{SOURCE2}
cd -
mkdir -p .cargo
cat > .cargo/config <<EOL
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "`pwd`/my_rust_vendor"
EOL

export CARGO_HOME=.cargo
cargo install cbindgen
export PATH=`pwd`/.cargo/bin:$PATH
export CBINDGEN=`pwd`/.cargo/bin/cbindgen


%if %{?system_sqlite}
# Do not proceed with build if the sqlite require would be broken:
# make sure the minimum requirement is non-empty, ...
sqlite_version=$(expr "%{sqlite_version}" : '\([0-9]*\.\)[0-9]*\.') || exit 1
# ... and that major number of the computed build-time version matches:
case "%{sqlite_build_version}" in
  "$sqlite_version"*) ;;
  *) exit 1 ;;
esac
%endif

# debug missing sqlite3 python module
./mach python -c "import sys;print(sys.path)"

%if 0%{?big_endian}
  echo "Generate big endian version of config/external/icu/data/icud58l.dat"
  icupkg -tb config/external/icu/data/icudt67l.dat config/external/icu/data/icudt67b.dat
  ls -l config/external/icu/data
  rm -f config/external/icu/data/icudt*l.dat
%endif

mkdir %{_buildrootdir}/bin || :
cp %{SOURCE28} %{_buildrootdir}/bin || :
chmod +x %{_buildrootdir}/bin/node-stdout-nonblocking-wrapper

# Update the various config.guess to upstream release for aarch64 support
find ./ -name config.guess -exec cp /usr/lib/rpm/config.guess {} ';'

# -fpermissive is needed to build with gcc 4.6+ which has become stricter
#
# Mozilla builds with -Wall with exception of a few warnings which show up
# everywhere in the code; so, don't override that.
#
# Disable C++ exceptions since Mozilla code is not exception-safe
#
MOZ_OPT_FLAGS=$(echo "%{optflags}" | %{__sed} -e 's/-Wall//')
#rhbz#1037063
# -Werror=format-security causes build failures when -Wno-format is explicitly given
# for some sources
# Explicitly force the hardening flags for Firefox so it passes the checksec test;
# See also https://fedoraproject.org/wiki/Changes/Harden_All_Packages
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -Wformat-security -Wformat -Werror=format-security"
%if 0%{?fedora} > 23
# Disable null pointer gcc6 optimization in gcc6 (rhbz#1328045)
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fno-delete-null-pointer-checks"
%endif
# Use hardened build?
%if %{?hardened_build}
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fPIC -Wl,-z,relro -Wl,-z,now"
%endif
%if %{?debug_build}
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-O2//')
%endif
%ifarch s390
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-g/-g1/')
# If MOZ_DEBUG_FLAGS is empty, firefox's build will default it to "-g" which
# overrides the -g1 from line above and breaks building on s390
# (OOM when linking, rhbz#1238225)
export MOZ_DEBUG_FLAGS=" "
%endif

# We don't wantfirefox to use CK_GCM_PARAMS_V3 in nss
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -DNSS_PKCS11_3_0_STRICT"

%if !%{build_with_clang}
%ifarch s390 %{arm} ppc aarch64 i686 x86_64 s390x
MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--reduce-memory-overheads"
%endif
%ifarch %{arm} i686
MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--strip-debug"
echo "ac_add_options --enable-linker=gold" >> .mozconfig
%endif
%endif

%ifarch %{arm} i686
export RUSTFLAGS="-Cdebuginfo=0"
%endif
export CFLAGS=$MOZ_OPT_FLAGS
export CXXFLAGS=$MOZ_OPT_FLAGS
export LDFLAGS=$MOZ_LINK_FLAGS

export PREFIX='%{_prefix}'
export LIBDIR='%{_libdir}'

%if %{build_with_clang}
echo "export LLVM_PROFDATA=\"llvm-profdata\"" >> .mozconfig
echo "export AR=\"llvm-ar\"" >> .mozconfig
echo "export NM=\"llvm-nm\"" >> .mozconfig
echo "export RANLIB=\"llvm-ranlib\"" >> .mozconfig
%else
echo "export CC=gcc" >> .mozconfig
echo "export CXX=g++" >> .mozconfig
echo "export AR=\"gcc-ar\"" >> .mozconfig
echo "export NM=\"gcc-nm\"" >> .mozconfig
echo "export RANLIB=\"gcc-ranlib\"" >> .mozconfig
%endif

MOZ_SMP_FLAGS=-j1
# More than two build tasks can lead to OOM gcc crash.
%if 0%{?rhel} < 8
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
%else
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le aarch64
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
[ "$RPM_BUILD_NCPUS" -ge 4 ] && MOZ_SMP_FLAGS=-j3
[ "$RPM_BUILD_NCPUS" -ge 8 ] && MOZ_SMP_FLAGS=-j3
%endif
%endif

cat /proc/meminfo

# Free memory in kB
if grep -q MemAvailable /proc/meminfo; then
    MEM_AVAILABLE=$(grep MemAvailable /proc/meminfo | awk '{ print $2 }')
else
    MEM_AVAILABLE=$(grep MemFree /proc/meminfo | awk '{ print $2 }')
fi

# Usually the compiler processes can take 2 GB of memory at peaks
TASK_SIZE=4000000
MEM_CONSTRAINED_JOBS=$(( MEM_AVAILABLE / TASK_SIZE ))

if [ $MEM_CONSTRAINED_JOBS -le 0 ]; then
  MEM_CONSTRAINED_JOBS=1
fi

CPU_AVAILABLE=$(/usr/bin/getconf _NPROCESSORS_ONLN)
# Pick the minimum from available CPUs or memory constrained number of jobs
MOZ_SMP_FLAGS=-j$([ "$CPU_AVAILABLE" -le "$MEM_CONSTRAINED_JOBS" ] && echo "$CPU_AVAILABLE" || echo "$MEM_CONSTRAINED_JOBS")
#MOZ_SMP_FLAGS=-j16

%if 0%{?bundle_gtk3}
# gtk3-private-setup-flags-env.inc
%include_file %{SOURCE205}
%endif

%if 0%{?bundle_nss}
echo "Setting nss flags"
# nss-setup-flags-env.inc
%include_file %{SOURCE401}
export PATH=%{_buildrootdir}/%{gtk3_install_path}/bin:$PATH
echo $PKG_CONFIG_PATH
%endif

export MOZ_MAKE_FLAGS="$MOZ_SMP_FLAGS"
export MOZ_SERVICES_SYNC="1"
# we need to strip the sources on i686 because to we don't use rpm to generate debugsymbols because of oom
%ifnarch i686 i386
export STRIP=/bin/true
%endif
which node
echo 'export NODEJS="%{_buildrootdir}/bin/node-stdout-nonblocking-wrapper"'
env
ls %{_buildrootdir}


%if 0%{?use_llvmts}
scl enable llvm-toolset-%{llvm_version} './mach build -v'
%else
./mach build -v
%endif

# create debuginfo for crash-stats.mozilla.com
%if %{enable_mozilla_crashreporter}
#cd %{moz_objdir}
make -C objdir buildsymbols
%endif

%if %{?run_tests}
%if %{?system_nss}
ln -s /usr/bin/certutil objdir/dist/bin/certutil
ln -s /usr/bin/pk12util objdir/dist/bin/pk12util

%endif
mkdir test_results
./mach --log-no-times check-spidermonkey &> test_results/check-spidermonkey || true
./mach --log-no-times check-spidermonkey &> test_results/check-spidermonkey-2nd-run || true
./mach --log-no-times cppunittest &> test_results/cppunittest || true
xvfb-run ./mach --log-no-times crashtest &> test_results/crashtest || true
./mach --log-no-times gtest &> test_results/gtest || true
xvfb-run ./mach --log-no-times jetpack-test &> test_results/jetpack-test || true
# not working right now ./mach marionette-test &> test_results/marionette-test || true
xvfb-run ./mach --log-no-times mochitest-a11y &> test_results/mochitest-a11y || true
xvfb-run ./mach --log-no-times mochitest-browser &> test_results/mochitest-browser || true
xvfb-run ./mach --log-no-times mochitest-chrome &> test_results/mochitest-chrome || true
xvfb-run ./mach --log-no-times mochitest-devtools &> test_results/mochitest-devtools || true
xvfb-run ./mach --log-no-times mochitest-plain &> test_results/mochitest-plain || true
xvfb-run ./mach --log-no-times reftest &> test_results/reftest || true
xvfb-run ./mach --log-no-times webapprt-test-chrome &> test_results/webapprt-test-chrome || true
xvfb-run ./mach --log-no-times webapprt-test-content &> test_results/webapprt-test-content || true
./mach --log-no-times webidl-parser-test &> test_results/webidl-parser-test || true
xvfb-run ./mach --log-no-times xpcshell-test &> test_results/xpcshell-test || true
%if %{?system_nss}
rm -f  objdir/dist/bin/certutil
rm -f  objdir/dist/bin/pk12util
%endif

%endif
#---------------------------------------------------------------------

%install

function install_rpms_to_current_dir() {
    PACKAGE_RPM=$(eval echo $1)
    PACKAGE_DIR=%{_rpmdir}

    if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
        # Hack for tps tests
        ARCH_STR=%{_arch}
        %ifarch i386 i686
            ARCH_STR="i?86"
        %endif
        PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
     fi

     for package in $(ls $PACKAGE_DIR/$PACKAGE_RPM)
     do
         echo "$package"
         rpm2cpio "$package" | cpio -idu
     done
}

%if 0%{?bundle_gtk3}
pushd %{buildroot}
# Install gtk3-private again to the buildroot, but without devel subpackage
install_rpms_to_current_dir gtk3-private-%{gtk3_nvr}*.rpm
install_rpms_to_current_dir gtk3-private-rpm-scripts-%{gtk3_nvr}*.rpm
popd
%endif

%if 0%{?bundle_nss}
  pushd %{buildroot}
  #install_rpms_to_current_dir nss-*.rpm
  install_rpms_to_current_dir nspr-4*.rpm
  install_rpms_to_current_dir nss-3*.rpm
  install_rpms_to_current_dir nss-softokn-3*.rpm
  install_rpms_to_current_dir nss-softokn-freebl-3*.rpm
  install_rpms_to_current_dir nss-util-3*.rpm
 
  # cleanup unecessary nss files
  #rm -rf %{_buildrootdir}/%{gtk3_install_path}/bin
  #rm -rf %{_buildrootdir}/%{gtk3_install_path}/include
  rm -rf %{buildroot}/%{gtk3_install_path}/lib/dracut
  rm -rf %{buildroot}/%{gtk3_install_path}/%{_lib}/nss
  #rm -rf %{_buildrootdir}/%{gtk3_install_path}/%{_lib}/pkgconfig
  rm -rf %{buildroot}/%{gtk3_install_path}/%{_lib}/share
  rm -rf %{buildroot}/%{gtk3_install_path}/share
  rm -rf %{buildroot}/etc/pki
  rm -rf %{buildroot}/usr/lib/.build-id
  rm -rf %{buildroot}/etc/crypto-policies
  popd
%endif

# Install bundled libffi
%if %{use_bundled_ffi}
  pushd %{buildroot}
  install_rpms_to_current_dir libffi-3*.rpm
  popd
%endif

# set up our default bookmarks
%{__cp} -p %{default_bookmarks_file} objdir/dist/bin/browser/chrome/en-US/locale/browser/bookmarks.html

# Make sure locale works for langpacks
%{__cat} > objdir/dist/bin/browser/defaults/preferences/firefox-l10n.js << EOF
pref("general.useragent.locale", "chrome://global/locale/intl.properties");
EOF

DESTDIR=%{buildroot} make -C objdir install

%{__mkdir_p} %{buildroot}{%{_libdir},%{_bindir},%{_datadir}/applications}

desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE20}

# set up the firefox start script
%{__rm} -rf %{buildroot}%{_bindir}/firefox
%{__cat} %{SOURCE21} > %{buildroot}%{_bindir}/firefox
sed -i -e 's|%PREFIX%|%{_prefix}|' %{buildroot}%{_bindir}/firefox
%if 0%{?bundle_gtk3}
sed -i -e 's|%RHEL_ENV_VARS%|export XDG_DATA_DIRS="$MOZ_LIB_DIR/firefox/bundled/share:/usr/share:$XDG_DATA_DIRS"\nexport FONTCONFIG_FILE="$MOZ_LIB_DIR/firefox/bundled/etc/fonts/fonts.conf"|' %{buildroot}%{_bindir}/firefox
%else
sed -i -e 's|%RHEL_ENV_VARS%||' %{buildroot}%{_bindir}/firefox
%endif

%{__chmod} 755 %{buildroot}%{_bindir}/firefox

%{__install} -p -D -m 644 %{SOURCE23} %{buildroot}%{_mandir}/man1/firefox.1

%{__rm} -f %{buildroot}/%{mozappdir}/firefox-config
%{__rm} -f %{buildroot}/%{mozappdir}/update-settings.ini

for s in 16 22 24 32 48 256; do
    %{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps
    %{__cp} -p browser/branding/official/default${s}.png \
               %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/firefox.png
done

# Install hight contrast icon
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps
%{__cp} -p %{SOURCE25} \
           %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps

# Register as an application to be visible in the software center
#
# NOTE: It would be *awesome* if this file was maintained by the upstream
# project, translated and installed into the right place during `make install`.
#
# See http://www.freedesktop.org/software/appstream/docs/ for more details.
#
%{__mkdir_p} %{buildroot}%{_datadir}/appdata
cat > %{buildroot}%{_datadir}/appdata/%{name}.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2014 Richard Hughes <richard@hughsie.com> -->
<!--
BugReportURL: https://bugzilla.mozilla.org/show_bug.cgi?id=1071061
SentUpstream: 2014-09-22
-->
<application>
  <id type="desktop">firefox.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <description>
    <p>
      Bringing together all kinds of awesomeness to make browsing better for you.
      Get to your favorite sites quickly – even if you don’t remember the URLs.
      Type your term into the location bar (aka the Awesome Bar) and the autocomplete
      function will include possible matches from your browsing history, bookmarked
      sites and open tabs.
    </p>
    <!-- FIXME: Needs another couple of paragraphs -->
  </description>
  <url type="homepage">http://www.mozilla.org/</url>
  <screenshots>
    <screenshot type="default">https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/firefox/a.png</screenshot>
    <screenshot>https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/firefox/b.png</screenshot>
    <screenshot>https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/firefox/c.png</screenshot>
  </screenshots>
  <!-- FIXME: change this to an upstream email address for spec updates
  <updatecontact>someone_who_cares@upstream_project.org</updatecontact>
   -->
</application>
EOF

echo > %{name}.lang
%if %{build_langpacks}
# Extract langpacks, make any mods needed, repack the langpack, and install it.
%{__mkdir_p} %{buildroot}%{langpackdir}
%{__tar} xf %{SOURCE1}
for langpack in `ls firefox-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionID=langpack-$language@firefox.mozilla.org
  %{__mkdir_p} $extensionID
  unzip -qq $langpack -d $extensionID
  find $extensionID -type f | xargs chmod 644

  cd $extensionID
  zip -qq -r9mX ../${extensionID}.xpi *
  cd -

  %{__install} -m 644 ${extensionID}.xpi %{buildroot}%{langpackdir}
  language=`echo $language | sed -e 's/-/_/g'`
  echo "%%lang($language) %{langpackdir}/${extensionID}.xpi" >> %{name}.lang
done
%{__rm} -rf firefox-langpacks

# Install langpack workaround (see #707100, #821169)
function create_default_langpack() {
language_long=$1
language_short=$2
cd %{buildroot}%{langpackdir}
ln -s langpack-$language_long@firefox.mozilla.org.xpi langpack-$language_short@firefox.mozilla.org.xpi
cd -
echo "%%lang($language_short) %{langpackdir}/langpack-$language_short@firefox.mozilla.org.xpi" >> %{name}.lang
}

# Table of fallbacks for each language
# please file a bug at bugzilla.redhat.com if the assignment is incorrect
create_default_langpack "es-AR" "es"
create_default_langpack "fy-NL" "fy"
create_default_langpack "ga-IE" "ga"
create_default_langpack "gu-IN" "gu"
create_default_langpack "hi-IN" "hi"
create_default_langpack "hy-AM" "hy"
create_default_langpack "nb-NO" "nb"
create_default_langpack "nn-NO" "nn"
create_default_langpack "pa-IN" "pa"
create_default_langpack "pt-PT" "pt"
create_default_langpack "sv-SE" "sv"
create_default_langpack "zh-TW" "zh"
%endif

# Keep compatibility with the old preference location.
%{__mkdir_p} %{buildroot}%{mozappdir}/defaults/preferences
%{__mkdir_p} %{buildroot}%{mozappdir}/browser/defaults
ln -s %{mozappdir}/defaults/preferences $RPM_BUILD_ROOT/%{mozappdir}/browser/defaults/preferences
# Default preferences
%{__cp} %{SOURCE12} %{buildroot}%{mozappdir}/defaults/preferences/all-redhat.js

# System config dir
%{__mkdir_p} %{buildroot}/%{_sysconfdir}/%{name}/pref

# System extensions
%{__mkdir_p} %{buildroot}%{_datadir}/mozilla/extensions/%{firefox_app_id}
%{__mkdir_p} %{buildroot}%{_libdir}/mozilla/extensions/%{firefox_app_id}

# Copy over the LICENSE
%{__install} -p -c -m 644 LICENSE %{buildroot}/%{mozappdir}

# Use the system hunspell dictionaries
%{__rm} -rf %{buildroot}%{mozappdir}/dictionaries
ln -s %{_datadir}/myspell %{buildroot}%{mozappdir}/dictionaries

# Enable crash reporter for Firefox application
%if %{enable_mozilla_crashreporter}
sed -i -e "s/\[Crash Reporter\]/[Crash Reporter]\nEnabled=1/" %{buildroot}/%{mozappdir}/application.ini
# Add debuginfo for crash-stats.mozilla.com
%{__mkdir_p} %{buildroot}/%{moz_debug_dir}
%{__cp} objdir/dist/%{symbols_file_name} %{buildroot}/%{moz_debug_dir}
%endif

%if %{run_tests}
# Add debuginfo for crash-stats.mozilla.com
%{__mkdir_p} %{buildroot}/test_results
%{__cp} test_results/* %{buildroot}/test_results
%endif


# Copy over run-mozilla.sh
%{__cp} build/unix/run-mozilla.sh %{buildroot}%{mozappdir}

# Add distribution.ini
%{__mkdir_p} %{buildroot}%{mozappdir}/distribution
%{__cp} %{SOURCE26} %{buildroot}%{mozappdir}/distribution

# Remove copied libraries to speed up build
rm -f %{buildroot}%{mozappdirdev}/sdk/lib/libmozjs.so
rm -f %{buildroot}%{mozappdirdev}/sdk/lib/libmozalloc.so
rm -f %{buildroot}%{mozappdirdev}/sdk/lib/libxul.so

%if %{bundle_gnome_extension}
# Gnome extension
%{__mkdir_p} %{buildroot}%{mozappdir}/distribution/extensions
%{__cp} -p %{SOURCE2} %{buildroot}%{mozappdir}/distribution/extensions/chrome-gnome-shell@gnome.org.xpi
chmod 644 %{buildroot}%{mozappdir}/distribution/extensions/chrome-gnome-shell@gnome.org.xpi
%endif
#---------------------------------------------------------------------

%preun
# is it a final removal?
if [ $1 -eq 0 ]; then
  %{__rm} -rf %{mozappdir}/components
  %{__rm} -rf %{mozappdir}/extensions
  %{__rm} -rf %{mozappdir}/plugins
fi

%clean
rm -rf %{_srcrpmdir}/gtk3-private-%{gtk3_nvr}*.src.rpm
find %{_rpmdir} -name "gtk3-private-*%{gtk3_nvr}*.rpm" -delete
rm -rf %{_srcrpmdir}/libffi*.src.rpm
find %{_rpmdir} -name "libffi*.rpm" -delete
rm -rf %{_srcrpmdir}/openssl*.src.rpm
find %{_rpmdir} -name "openssl*.rpm" -delete
rm -rf %{_srcrpmdir}/nss*.src.rpm
find %{_rpmdir} -name "nss*.rpm" -delete
rm -rf %{_srcrpmdir}/nspr*.src.rpm
find %{_rpmdir} -name "nspr*.rpm" -delete

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
%if 0%{?bundle_gtk3}
# gtk3-private-post.inc
%include_file %{SOURCE201}
%endif

%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
%if 0%{?bundle_gtk3}
# gtk3-private-postun.inc
%include_file %{SOURCE202}
%endif

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
%if 0%{?bundle_gtk3}
# gtk3-private-posttrans.inc
%include_file %{SOURCE203}
%endif

%files -f %{name}.lang
%{_bindir}/firefox
%{mozappdir}/firefox
%{mozappdir}/firefox-bin
%doc %{_mandir}/man1/*
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/*
%dir %{_datadir}/mozilla/extensions/*
%dir %{_libdir}/mozilla/extensions/*
%{_datadir}/appdata/*.appdata.xml
%{_datadir}/applications/*.desktop
%dir %{mozappdir}
%doc %{mozappdir}/LICENSE
%{mozappdir}/browser/chrome
%{mozappdir}/defaults/preferences/*
%{mozappdir}/browser/defaults/preferences
%{mozappdir}/browser/features/*.xpi
%{mozappdir}/distribution/distribution.ini
%if %{build_langpacks}
%dir %{langpackdir}
%endif
%{mozappdir}/browser/omni.ja
%{mozappdir}/run-mozilla.sh
%{mozappdir}/application.ini
%{mozappdir}/pingsender
%exclude %{mozappdir}/removed-files
%{_datadir}/icons/hicolor/16x16/apps/firefox.png
%{_datadir}/icons/hicolor/22x22/apps/firefox.png
%{_datadir}/icons/hicolor/24x24/apps/firefox.png
%{_datadir}/icons/hicolor/256x256/apps/firefox.png
%{_datadir}/icons/hicolor/32x32/apps/firefox.png
%{_datadir}/icons/hicolor/48x48/apps/firefox.png
%{_datadir}/icons/hicolor/symbolic/apps/firefox-symbolic.svg
%if %{enable_mozilla_crashreporter}
%{mozappdir}/crashreporter
%{mozappdir}/crashreporter.ini
%{mozappdir}/minidump-analyzer
%{mozappdir}/Throbber-small.gif
%{mozappdir}/browser/crashreporter-override.ini
%endif
%{mozappdir}/*.so
%{mozappdir}/gtk2/*.so
%{mozappdir}/defaults/pref/channel-prefs.js
%{mozappdir}/dependentlibs.list
%{mozappdir}/dictionaries
%{mozappdir}/omni.ja
%{mozappdir}/platform.ini
%{mozappdir}/plugin-container
%{mozappdir}/gmp-clearkey
%{mozappdir}/fonts/*.ttf
%if !%{?system_nss}
%exclude %{mozappdir}/libnssckbi.so
%endif
%if 0%{use_bundled_ffi}
%{mozappdir}/bundled/%{_lib}/libffi.so*
%exclude %{_datadir}/doc/libffi*
%endif

%if 0%{?bundle_gtk3}
# gtk3-private-files.inc
%include_file %{SOURCE204}
%endif

%if 0%{?bundle_nss}
%{mozappdir}/bundled/%{_lib}/libfreebl*
%{mozappdir}/bundled/%{_lib}/libnss3*
%{mozappdir}/bundled/%{_lib}/libnssdbm3*
%{mozappdir}/bundled/%{_lib}/libnssutil3*
%{mozappdir}/bundled/%{_lib}/libsmime3*
%{mozappdir}/bundled/%{_lib}/libsoftokn*
%{mozappdir}/bundled/%{_lib}/libssl3*
%{mozappdir}/bundled/%{_lib}/libnspr4.so
%{mozappdir}/bundled/%{_lib}/libplc4.so
%{mozappdir}/bundled/%{_lib}/libplds4.so
%endif


#---------------------------------------------------------------------

%changelog
* Thu Apr 15 2021 Mohan Boddu <mboddu@redhat.com> - 78.8.0-5
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Mon Mar 01 2021 Jan Horak <jhorak@redhat.com> - 78.8.0-4
- Removed autoconf213 dependency

* Mon Feb 22 2021 Jan Horak <jhorak@redhat.com> - 78.8.0-2
- Update to 78.8.0 build2
- Removed dependency on gconf-2

* Wed Jan  6 2021 Eike Rathke <erack@redhat.com> - 78.6.1-1
- Update to 78.6.1 build1

* Thu Dec 10 2020 Jan Horak <jhorak@redhat.com> - 78.6.0-1
- Update to 78.6.0 build1

* Wed Nov 18 2020 Jan Horak <jhorak@redhat.com> - 78.5.0-1
- Update to 78.5.0 build1

* Tue Nov 10 2020 erack@redhat.com - 78.4.1-1
- Update to 78.4.1

* Tue Nov 10 2020 Jan Horak <jhorak@redhat.com> - 78.4.0-3
- Fixing flatpak build, fixing firefox.sh.in to not disable langpacks loading

* Thu Oct 29 2020 Jan Horak <jhorak@redhat.com> - 78.4.0-2
- Enable addon sideloading

* Fri Oct 16 2020 Jan Horak <jhorak@redhat.com> - 78.4.0-1
- Update to 78.4.0 build2

* Fri Sep 18 2020 Jan Horak <jhorak@redhat.com>
- Update to 78.3.0 build1

* Tue Aug 18 2020 Jan Horak <jhorak@redhat.com> - 78.2.0-3
- Update to 78.2.0 build1

* Fri Jul 24 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.11.0 build1

* Fri Jun 26 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.10.0 build1

* Fri May 29 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.9.0 build1
- Added patch for pipewire 0.3

* Mon May 11 2020 Jan Horak <jhorak@redhat.com>
- Added s390x specific patches

* Wed Apr 29 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.8.0 build1

* Thu Apr 23 2020 Martin Stransky <stransky@redhat.com> - 68.7.0-3
- Added fix for rhbz#1821418

* Tue Apr 07 2020 Jan Horak <jhorak@redhat.com> - 68.7.0-2
- Update to 68.7.0 build3

* Mon Apr  6 2020 Jan Horak <jhorak@redhat.com> - 68.6.1-1
- Update to 68.6.1 ESR

* Wed Mar 04 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.6.0 build1

* Mon Feb 24 2020 Martin Stransky <stransky@redhat.com> - 68.5.0-3
- Added fix for rhbz#1805667
- Enabled mzbz@1170092 - Firefox prefs at /etc

* Fri Feb 07 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.5.0 build2

* Wed Feb 05 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.5.0 build1

* Wed Jan 08 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.4.1esr build1

* Fri Jan 03 2020 Jan Horak <jhorak@redhat.com>
- Update to 68.4.0esr build1

* Wed Dec 18 2019 Jan Horak <jhorak@redhat.com>
- Fix for wrong intl.accept_lang when using non en-us langpack

* Wed Nov 27 2019 Martin Stransky <stransky@redhat.com> - 68.3.0-1
- Update to 68.3.0 ESR

* Thu Oct 24 2019 Martin Stransky <stransky@redhat.com> - 68.2.0-4
- Added patch for TLS 1.3 support.

* Wed Oct 23 2019 Martin Stransky <stransky@redhat.com> - 68.2.0-3
- Rebuild

* Mon Oct 21 2019 Martin Stransky <stransky@redhat.com> - 68.2.0-2
- Rebuild

* Thu Oct 17 2019 Martin Stransky <stransky@redhat.com> - 68.2.0-1
- Update to 68.2.0 ESR

* Thu Oct 10 2019 Martin Stransky <stransky@redhat.com> - 68.1.0-6
- Enable system nss on RHEL6

* Thu Sep  5 2019 Jan Horak <jhorak@redhat.com> - 68.1.0-2
- Enable building langpacks

* Wed Aug 28 2019 Jan Horak <jhorak@redhat.com> - 68.1.0-1
- Update to 68.1.0 ESR

* Mon Aug 5 2019 Martin Stransky <stransky@redhat.com> - 68.0.1-4
- Enable system nss

* Mon Jul 29 2019 Martin Stransky <stransky@redhat.com> - 68.0.1-3
- Enable official branding

* Fri Jul 26 2019 Martin Stransky <stransky@redhat.com> - 68.0.1-2
- Enabled PipeWire on RHEL8

* Fri Jul 26 2019 Martin Stransky <stransky@redhat.com> - 68.0.1-1
- Updated to 68.0.1 ESR

* Tue Jul 16 2019 Jan Horak <jhorak@redhat.com> - 68.0-0.11
- Update to 68.0 ESR

* Tue Jun 25 2019 Martin Stransky <stransky@redhat.com> - 68.0-0.10
- Updated to 68.0 alpha 13
- Enabled second arches

* Fri Mar 22 2019 Martin Stransky <stransky@redhat.com> - 68.0-0.1
- Updated to 68.0 alpha

* Fri Mar 15 2019 Martin Stransky <stransky@redhat.com> - 60.6.0-3
- Added Google API keys (mozbz#1531176)

* Thu Mar 14 2019 Martin Stransky <stransky@redhat.com> - 60.6.0-2
- Update to 60.6.0 ESR (Build 2)

* Wed Mar 13 2019 Martin Stransky <stransky@redhat.com> - 60.6.0-1
- Update to 60.6.0 ESR (Build 1)

* Wed Feb 13 2019 Jan Horak <jhorak@redhat.com> - 60.5.1-1
- Update to 60.5.1 ESR

* Wed Feb 6 2019 Martin Stransky <stransky@redhat.com> - 60.5.0-3
- Added fix for rhbz#1672424 - Firefox crashes on NFS drives.

* Fri Jan 25 2019 Martin Stransky <stransky@redhat.com> - 60.5.0-2
- Updated to 60.5.0 ESR build2

* Tue Jan 22 2019 Martin Stransky <stransky@redhat.com> - 60.5.0-1
- Updated to 60.5.0 ESR build1

* Thu Jan 10 2019 Jan Horak <jhorak@redhat.com> - 60.4.0-3
- Fixing fontconfig warnings (rhbz#1601475)

* Wed Jan  9 2019 Jan Horak <jhorak@redhat.com> - 60.4.0-2
- Added pipewire patch from Tomas Popela (rhbz#1664270)

* Wed Dec  5 2018 Jan Horak <jhorak@redhat.com> - 60.4.0-1
- Update to 60.4.0 ESR

* Tue Dec  4 2018 Jan Horak <jhorak@redhat.com> - 60.3.0-2
- Added firefox-gnome-shell-extension

* Fri Oct 19 2018 Jan Horak <jhorak@redhat.com> - 60.3.0-1
- Update to 60.3.0 ESR

* Wed Oct 10 2018 Jan Horak <jhorak@redhat.com> - 60.2.2-2
- Added patch for rhbz#1633932

* Tue Oct  2 2018 Jan Horak <jhorak@redhat.com> - 60.2.2-1
- Update to 60.2.2 ESR

* Mon Sep 24 2018 Jan Horak <jhorak@redhat.com> - 60.2.1-1
- Update to 60.2.1 ESR

* Fri Aug 31 2018 Jan Horak <jhorak@redhat.com> - 60.2.0-1
- Update to 60.2.0 ESR

* Tue Aug 28 2018 Jan Horak <jhorak@redhat.com> - 60.1.0-9
- Do not set user agent (rhbz#1608065)
- GTK dialogs are localized now (rhbz#1619373)
- JNLP association works again (rhbz#1607457)

* Thu Aug 16 2018 Jan Horak <jhorak@redhat.com> - 60.1.0-8
- Fixed homepage and bookmarks (rhbz#1606778)
- Fixed missing file associations in RHEL6 (rhbz#1613565)

* Thu Jul 12 2018 Jan Horak <jhorak@redhat.com> - 60.1.0-7
- Run at-spi-bus if not running already (for the bundled gtk3)

* Mon Jul  9 2018 Jan Horak <jhorak@redhat.com> - 60.1.0-6
- Fix for missing schemes for bundled gtk3

* Mon Jun 25 2018 Martin Stransky <stransky@redhat.com> - 60.1.0-5
- Added mesa-libEGL dependency to gtk3/rhel6

* Sun Jun 24 2018 Martin Stransky <stransky@redhat.com> - 60.1.0-4
- Disabled jemalloc on all second arches

* Fri Jun 22 2018 Martin Stransky <stransky@redhat.com> - 60.1.0-3
- Updated to 60.1.0 ESR build2

* Thu Jun 21 2018 Martin Stransky <stransky@redhat.com> - 60.1.0-2
- Disabled jemalloc on second arches

* Wed Jun 20 2018 Martin Stransky <stransky@redhat.com> - 60.1.0-1
- Updated to 60.1.0 ESR

* Wed Jun 13 2018 Jan Horak <jhorak@redhat.com> - 60.0-12
- Fixing bundled libffi issues
- Readded some requirements

* Mon Jun 11 2018 Martin Stransky <stransky@redhat.com> - 60.0-10
- Added fix for mozilla BZ#1436242 - IPC crashes.

* Mon Jun 11 2018 Jan Horak <jhorak@redhat.com> - 60.0-9
- Bundling libffi for the sec-arches
- Added openssl-devel for the Python
- Fixing bundled gtk3

* Fri May 18 2018 Martin Stransky <stransky@redhat.com> - 60.0-8
- Added fix for mozilla BZ#1458492

* Wed May 16 2018 Martin Stransky <stransky@redhat.com> - 60.0-7
- Added patch from rhbz#1498561 to fix ppc64(le) crashes.

* Wed May 16 2018 Martin Stransky <stransky@redhat.com> - 60.0-6
- Disabled jemalloc on second arches

* Sun May  6 2018 Jan Horak <jhorak@redhat.com> - 60.0-4
- Update to 60.0 ESR

* Thu Mar  8 2018 Jan Horak <jhorak@redhat.com> - 52.7.0-1
- Update to 52.7.0 ESR

* Mon Jan 29 2018 Martin Stransky <stransky@redhat.com> - 52.6.0-2
- Build Firefox for desktop arches only (x86_64 and ppc64le)

* Thu Jan 18 2018 Martin Stransky <stransky@redhat.com> - 52.6.0-1
- Update to 52.6.0 ESR

* Thu Nov  9 2017 Jan Horak <jhorak@redhat.com> - 52.5.0-1
- Update to 52.5.0 ESR

* Mon Sep 25 2017 Jan Horak <jhorak@redhat.com> - 52.4.0-1
- Update to 52.4.0 ESR

* Thu Aug  3 2017 Jan Horak <jhorak@redhat.com> - 52.3.0-3
- Update to 52.3.0 ESR (b2)
- Require correct nss version

* Tue Jun 13 2017 Jan Horak <jhorak@redhat.com> - 52.2.0-1
- Update to 52.2.0 ESR

* Wed May 24 2017 Jan Horak <jhorak@redhat.com> - 52.1.2-1
- Update to 52.1.2 ESR

* Wed May 24 2017 Jan Horak <jhorak@redhat.com> - 52.0-7
- Added fix for accept language (rhbz#1454322)

* Wed Mar 22 2017 Jan Horak <jhorak@redhat.com> - 52.0-6
- Removing patch required for older NSS from RHEL 7.3
- Added patch for rhbz#1414564

* Fri Mar 17 2017 Martin Stransky <stransky@redhat.com> - 52.0-5
- Added fix for mozbz#1348168/CVE-2017-5428

* Mon Mar  6 2017 Jan Horak <jhorak@redhat.com> - 52.0-4
- Update to 52.0 ESR (b4)

* Thu Mar 2 2017 Martin Stransky <stransky@redhat.com> - 52.0-3
- Added fix for rhbz#1423012 - ppc64 gfx crashes

* Wed Mar  1 2017 Jan Horak <jhorak@redhat.com> - 52.0-2
- Enable system nss

* Tue Feb 28 2017 Martin Stransky <stransky@redhat.com> - 52.0-1
- Update to 52.0ESR (B1)
- Build RHEL7 package for Gtk3

* Mon Feb 27 2017 Martin Stransky <stransky@redhat.com> - 52.0-0.13
- Added fix for rhbz#1414535

* Tue Feb 21 2017 Jan Horak <jhorak@redhat.com> - 52.0-0.12
- Update to 52.0b8

* Tue Feb  7 2017 Jan Horak <jhorak@redhat.com> - 52.0-0.11
- Readded addons patch

* Mon Feb  6 2017 Jan Horak <jhorak@redhat.com> - 52.0-0.10
- Update to 52.0b3

* Tue Jan 31 2017 Jan Horak <jhorak@redhat.com> - 52.0-0.9
- Update to 52.0b2

* Fri Jan 27 2017 Jan Horak <jhorak@redhat.com> - 52.0-0.8
- Update to 52.0b1

* Thu Dec  8 2016 Jan Horak <jhorak@redhat.com> - 52.0-0.5
- Firefox Aurora 52 testing build
